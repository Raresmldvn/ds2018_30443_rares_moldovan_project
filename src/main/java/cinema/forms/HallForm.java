package cinema.forms;

public class HallForm {

	private int id;
	private String name;
	private int nrOfPlaces;
	private boolean generateSeats;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNrOfPlaces() {
		return nrOfPlaces;
	}
	public void setNrOfPlaces(int nrOfPlaces) {
		this.nrOfPlaces = nrOfPlaces;
	}
	public boolean isGenerateSeats() {
		return generateSeats;
	}
	public void setGenerateSeats(boolean generateSeats) {
		this.generateSeats = generateSeats;
	}
	
	
}
