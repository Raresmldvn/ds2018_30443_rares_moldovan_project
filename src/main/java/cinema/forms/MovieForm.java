package cinema.forms;

import java.util.Set;

public class MovieForm {

	private Integer id;
	private String name;
	private Integer year;
	private String image;
	private String description;
	private Set<Integer> genres;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Integer> getGenres() {
		return genres;
	}

	public void setGenres(Set<Integer> genres) {
		this.genres = genres;
	}
	
	public void setImage(String image) {
		this.image = image;
	}
	
	public String getImage() {
		return this.image;
	}
}
