package cinema.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cinema.dto.MovieDTO;
import cinema.forms.MovieForm;
import cinema.services.MovieService;
import cinema.services.util.ChartService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/movie")
public class MovieController {

	@Autowired
	private MovieService movieService;
	
	@Autowired
	private ChartService chartService;
	
	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public MovieDTO getMovieById(@PathVariable("id") int id) {
		return movieService.findMovieById(id);
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<MovieDTO> getAllMovies() {
		return movieService.findAll();
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	@PreAuthorize(value ="hasAnyRole('ROLE_ADMIN')")
	public void deleteMovie(@PathVariable("id") int id) {
		movieService.deleteMovie(id);
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@PreAuthorize(value ="hasAnyRole('ROLE_ADMIN')")
	public int createMovie(@RequestBody MovieForm form) {
		return movieService.createMovie(form);
	}
	
	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	public int createMovie(@RequestBody MovieForm form, @PathVariable("id") int id) {
		return movieService.updateMovie(id, form);
	}
	
	@PreAuthorize(value ="hasAnyRole('ROLE_USER')")
	@RequestMapping(value = "/filter", method = RequestMethod.GET)
	public List<MovieDTO> getAllMovies(@RequestParam("page") String page, @RequestParam("year") String year, @RequestParam("order") String order, @RequestParam("date") String date, @RequestParam("genre") String genreId) {
		int yearInt, genre, pageInt;
		
		try {
			pageInt = Integer.parseInt(page);
		} catch(Exception e) {
			pageInt = 0;
		}
		try {
			yearInt = Integer.parseInt(year);
		} catch(Exception e) {
			yearInt = 0;
		}
		
		try {
			genre = Integer.parseInt(genreId);
		} catch(Exception e) {
			genre = 0;
		}
		return movieService.filter(pageInt, yearInt, order, date, genre);
	}
	
	@PreAuthorize(value ="hasAnyRole('ROLE_USER')")
	@RequestMapping(value = "/countfilter", method = RequestMethod.GET)
	public int getAllMovies( @RequestParam("year") String year, @RequestParam("order") String order, @RequestParam("date") String date, @RequestParam("genre") String genreId) {
		int yearInt, genre;
		
		try {
			yearInt = Integer.parseInt(year);
		} catch(Exception e) {
			yearInt = 0;
		}
		
		try {
			genre = Integer.parseInt(genreId);
		} catch(Exception e) {
			genre = 0;
		}
		return movieService.getNrPages(yearInt, order, date, genre);
	}
	@RequestMapping(value = "/count", method = RequestMethod.GET)
	public long getCount(@RequestParam("page") int page) {
		return movieService.findCount();
	}
	
	@PreAuthorize(value ="hasAnyRole('ROLE_USER')")
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public List<MovieDTO> getMovies(@RequestParam("query") String query) {
		return movieService.search(query);
	}
	
	@RequestMapping(value = "/chart", method = RequestMethod.GET)
	public void setChart(@RequestParam("start") String start, @RequestParam("end") String end, @RequestParam("name") String name) {
		chartService.generateChart(start, end, name);
	}
}

