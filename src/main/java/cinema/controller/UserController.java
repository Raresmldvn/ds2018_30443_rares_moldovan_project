package cinema.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import cinema.dto.UserDTO;
import cinema.services.UserService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public UserDTO getUserById(@PathVariable("id") int id) {
		return userService.findUserById(id);
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<UserDTO> getAllUsers() {
		return userService.findAll();
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@PreAuthorize(value ="hasAnyRole('ROLE_ADMIN')")
	public int insertUser(@RequestBody UserDTO userDTO) {
		return userService.create(userDTO);
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	@PreAuthorize(value ="hasAnyRole('ROLE_ADMIN')")
	public void deleteUser(@PathVariable("id") int id) {
		userService.deleteUser(id);
	}
	
	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	@PreAuthorize(value ="hasAnyRole('ROLE_ADMIN')")
	public int updateUser(@RequestBody UserDTO userDTO, @PathVariable("id") int id) {
		return userService.update(id, userDTO);
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public UserDTO logIn(@RequestBody UserDTO userDTO) {
		return userService.logIn(userDTO);
	}
}

