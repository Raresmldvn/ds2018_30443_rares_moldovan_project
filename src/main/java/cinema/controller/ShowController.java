package cinema.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cinema.dto.ShowDTO;
import cinema.services.ShowService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/show")
public class ShowController {

	@Autowired
	private ShowService showService;
	

	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public ShowDTO getShowById(@PathVariable("id") int id) {
		return showService.findShowById(id);
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<ShowDTO> getAllShows() {
		return showService.findAll();
	}
	
	@RequestMapping(value = "/movie", method = RequestMethod.GET)
	public List<ShowDTO> getShowsForMovie(@RequestParam("id") int id) {
		return showService.getShowsForMovie(id);
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	@PreAuthorize(value ="hasAnyRole('ROLE_ADMIN')")
	public void deleteShow(@PathVariable("id") int id) {
		showService.deleteShow(id);
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@PreAuthorize(value ="hasAnyRole('ROLE_ADMIN')")
	public int createShow(@RequestBody ShowDTO form) {
		return showService.createShow(form);
	}
	
	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	@PreAuthorize(value ="hasAnyRole('ROLE_ADMIN')")
	public int updateShow(@RequestBody ShowDTO form, @PathVariable("id") int id) {
		return showService.updateShow(id, form);
	}
	
	@RequestMapping(value = "/seats/{id}/", method = RequestMethod.GET)
	public int getRemainingSeats(@PathVariable("id") int id) {
		return showService.numberOfSeatsLeft(id);
	}
	
}
