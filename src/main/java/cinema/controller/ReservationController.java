package cinema.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cinema.dto.ReservationDTO;
import cinema.services.ReservationService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/reservation")
public class ReservationController {
	
	@Autowired
	private ReservationService reservationService;
	
	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public ReservationDTO getReservationById(@PathVariable("id") int id) {
		return reservationService.findReservationById(id);
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<ReservationDTO> getAllReservations() {
		return reservationService.findAll();
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	@PreAuthorize(value ="hasAnyRole('ROLE_ADMIN')")
	public void deleteReservation(@PathVariable("id") int id) {
		reservationService.deleteReservation(id);
	}
	
	@PreAuthorize(value ="hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public int insertReservation(@RequestBody ReservationDTO form) {
		return reservationService.createReservation(form);
	}
	
	@PreAuthorize(value ="hasAnyRole('ROLE_ADMIN')")
	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	public int updateReservation(@RequestBody ReservationDTO form, @PathVariable("id") int id) {
		return reservationService.updateReservation(id, form);
	}
}
