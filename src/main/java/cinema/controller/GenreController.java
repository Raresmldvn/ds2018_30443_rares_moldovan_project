package cinema.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cinema.dto.GenreDTO;
import cinema.services.GenreService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/genre")
public class GenreController {

	@Autowired
	private GenreService genreService;
	
	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public GenreDTO getGenreById(@PathVariable("id") int id) {
		return genreService.findGenreById(id);
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<GenreDTO> getAllMovies() {
		return genreService.findAll();
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public void deleteGenre(@PathVariable("id") int id) {
		genreService.deleteGenre(id);
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@PreAuthorize(value ="hasAnyRole('ROLE_ADMIN')")
	public int createGenre(@RequestParam("name") String name) {
		return genreService.createGenre(name);
	}
	
	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	@PreAuthorize(value ="hasAnyRole('ROLE_ADMIN')")
	public int updateGenre(@RequestParam("name") String name, @PathVariable("id") int id) {
		return genreService.updateGenre(id, name);
	}
}
