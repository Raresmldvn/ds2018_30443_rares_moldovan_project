package cinema.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cinema.dto.SeatDTO;
import cinema.services.SeatService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/seat")
public class SeatController {

	@Autowired
	private SeatService seatService;
	
	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public SeatDTO getHallById(@PathVariable("id") int id) {
		return seatService.findSeatById(id);
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<SeatDTO> getAllSeats() {
		return seatService.findAll();
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	@PreAuthorize(value ="hasAnyRole('ROLE_ADMIN')")
	public void deleteSeat(@PathVariable("id") int id) {
		seatService.deleteSeat(id);
	}
	
	@PreAuthorize(value ="hasAnyRole('ROLE_ADMIN')")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public int insertSeat(@RequestBody SeatDTO form) {
		return seatService.createSeat(form);
	}
	
	
	@PreAuthorize(value ="hasAnyRole('ROLE_ADMIN')")
	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	public int updateSeat(@RequestBody SeatDTO form, @PathVariable("id") int id) {
		return seatService.updateSeat(id, form);
	}
	
	@RequestMapping(value = "/show/{id}", method = RequestMethod.GET)
	public List<SeatDTO> getRemainingSeatsForShow(@PathVariable("id") int id) {
		return seatService.getRemainingSeats(id);
	}
	
	@RequestMapping(value = "/hall/{id}", method = RequestMethod.GET)
	public List<SeatDTO> getSeatsForHall(@PathVariable("id") int id) {
		return seatService.getSeatsForHall(id);
	}
}
