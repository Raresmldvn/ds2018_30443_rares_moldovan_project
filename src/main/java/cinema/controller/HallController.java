package cinema.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cinema.dto.HallDTO;
import cinema.forms.HallForm;
import cinema.services.HallService;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/hall")
public class HallController {

	@Autowired
	private HallService hallService;
	

	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public HallDTO getHallById(@PathVariable("id") int id) {
		return hallService.findHallById(id);
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<HallDTO> getAllHalls() {
		return hallService.findAll();
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
	public void deleteHall(@PathVariable("id") int id) {
		hallService.deleteHall(id);
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@PreAuthorize(value ="hasAnyRole('ROLE_ADMIN')")
	public int createMovie(@RequestBody HallForm form) {
		return hallService.createHall(form);
	}
	
	@RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
	@PreAuthorize(value ="hasAnyRole('ROLE_ADMIN')")
	public int createMovie(@RequestBody HallForm form, @PathVariable("id") int id) {
		return hallService.updateHall(id, form);
	}
}
