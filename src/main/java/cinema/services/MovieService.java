package cinema.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import cinema.dto.MovieDTO;
import cinema.dto.UserDTO;
import cinema.entities.Genre;
import cinema.entities.Movie;
import cinema.entities.Show;
import cinema.entities.User;
import cinema.errorhandler.ResourceNotFoundException;
import cinema.es.helper.Transport;
import cinema.es.model.MovieModel;
import cinema.forms.MovieForm;
import cinema.repositories.MovieRepository;
import cinema.services.filter.DateFilter;
import cinema.services.filter.Filter;
import cinema.services.filter.GenreFilter;
import cinema.services.filter.PaginationFilter;
import cinema.services.filter.YearFilter;

@Service
public class MovieService {

	@Autowired
	private MovieRepository movieRepository;
	
	private Transport esTransport;
	
	public MovieDTO findMovieById(int id) {
		Movie movie = movieRepository.findById(id);
		if(movie==null) {
			throw new ResourceNotFoundException(Movie.class.getSimpleName());
		}
		MovieDTO result = new MovieDTO.Builder()
				.id(movie.getId())
				.name(movie.getName())
				.description(movie.getDescription())
				.year(movie.getYear())
				.image(movie.getImage())
				.genres(extractGenreNames(movie.getGenres()))
				.shows(extractShowDates(movie.getShows()))
				.create();
		return result;
	}
	
	public List<MovieDTO> findAll() {
		List<Movie> movies = movieRepository.findAll();
		List<MovieDTO> toReturn = new ArrayList<MovieDTO>();
		for (Movie movie : movies) {
			MovieDTO result = new MovieDTO.Builder()
					.id(movie.getId())
					.name(movie.getName())
					.description(movie.getDescription())
					.year(movie.getYear())
					.image(movie.getImage())
					.genres(extractGenreNames(movie.getGenres()))
					.shows(extractShowDates(movie.getShows()))
					.create();
			toReturn.add(result);
		}
		return toReturn;
	}
	
	public List<MovieDTO> getPage(int page) {
		//List<Movie> movies = movieRepository.findByPage(0, page);
		Pageable limit = new PageRequest(0,5);
		Page<Movie> moviePages = movieRepository.findAll(limit);
		List<Movie> movies = moviePages.getContent();
		List<MovieDTO> toReturn = new ArrayList<MovieDTO>();
		for (Movie movie : movies) {
			MovieDTO result = new MovieDTO.Builder()
					.id(movie.getId())
					.name(movie.getName())
					.description(movie.getDescription())
					.year(movie.getYear())
					.image(movie.getImage())
					.genres(extractGenreNames(movie.getGenres()))
					.shows(extractShowDates(movie.getShows()))
					.create();
			toReturn.add(result);
		}
		return toReturn;
	}
	
	public void deleteMovie(int id) {
		movieRepository.delete(id);
	}
	
	public int createMovie(MovieForm form) {
		Movie movie = new Movie();
		movie.setName(form.getName());
		movie.setImage(form.getImage());
		movie.setYear(form.getYear());
		movie.setDescription(form.getDescription());
		Set<Integer> genreIds = form.getGenres();
		Set<Genre> genres = new HashSet<Genre>();
		for(Integer genreId : genreIds) {
			Genre genre = new Genre(genreId, "");
			genres.add(genre);
		}
		movie.setGenres(genres);
		Movie result = movieRepository.save(movie);
		
		//Index in ES
		esTransport = new Transport();
		MovieModel movieModel = new MovieModel(result.getId(), result.getDescription(), result.getName());
		System.out.println(esTransport.indexMovie(movieModel));
		return result.getId();
	}
	
	public int updateMovie(int id, MovieForm form) {
		Movie movie = new Movie();
		movie.setId(id);
		movie.setName(form.getName());
		movie.setImage(form.getImage());
		movie.setYear(form.getYear());
		movie.setDescription(form.getDescription());
		Set<Integer> genreIds = form.getGenres();
		Set<Genre> genres = new HashSet<Genre>();
		for(Integer genreId : genreIds) {
			Genre genre = new Genre(genreId, "");
			genres.add(genre);
		}
		movie.setGenres(genres);
		Movie result = movieRepository.save(movie);
		return result.getId();
	}
	
	public long findCount() {
		return movieRepository.count();
	}
	
	public List<MovieDTO> filter(int page, int year, String order, String date, int genreId) {
		List<Movie> initialList;
		if("ASC".equals(order)) {
			initialList = movieRepository.findAllByOrderByYearAsc();
		} else if("DESC".equals(order)) {
			initialList = movieRepository.findAllByOrderByYearDesc();
		} else {
			initialList = movieRepository.findAll();
		}
		List<Movie> filtered = initialList;
		Filter[] filters = {new YearFilter(filtered, year), new DateFilter(filtered, date), 
		                  new GenreFilter(filtered, genreId), new PaginationFilter(filtered, page)};
		for(int i=0; i<4; i++) {
			filters[i].setList(filtered);
			filtered = filters[i].filter();
		}
		List<MovieDTO> toReturn = new ArrayList<MovieDTO>();
		for (Movie movie : filtered) {
			MovieDTO result = new MovieDTO.Builder()
					.id(movie.getId())
					.name(movie.getName())
					.description(movie.getDescription())
					.year(movie.getYear())
					.image(movie.getImage())
					.genres(extractGenreNames(movie.getGenres()))
					.shows(extractShowDates(movie.getShows()))
					.create();
			toReturn.add(result);
		}
		return toReturn;
		
	}
	
	public List<MovieDTO> search(String queryString) {
		esTransport = new Transport();
		int[] ids = esTransport.searchMovie(queryString);
		List<MovieDTO> toReturn = new ArrayList<MovieDTO>();
		if(ids==null) {
			return toReturn;
		}
		for(int i=0; i<ids.length; i++) {
			Movie movie = movieRepository.findById(ids[i]);
			MovieDTO result = new MovieDTO.Builder()
					.id(movie.getId())
					.name(movie.getName())
					.description(movie.getDescription())
					.year(movie.getYear())
					.image(movie.getImage())
					.genres(extractGenreNames(movie.getGenres()))
					.shows(extractShowDates(movie.getShows()))
					.create();
			toReturn.add(result);
		}
		return toReturn;
	}
	
	public int getNrPages(int year, String order, String date, int genreId) {
		List<Movie> filtered = movieRepository.findAll();
		Filter[] filters = {new YearFilter(filtered, year), new DateFilter(filtered, date), 
		                  new GenreFilter(filtered, genreId)};
		for(int i=0; i<3; i++) {
			filters[i].setList(filtered);
			filtered = filters[i].filter();
		}
		return filtered.size();
	}
	
	private String[] extractGenreNames(Set<Genre> genres) {
		String[] names = new String[genres.size()];
		int i=0;
		for(Genre genre : genres) {
			names[i] = genre.getName();
			i++;
		}
		return names;
	}
	
	private Date[] extractShowDates(Set<Show> shows) {
		Date[] dates = new Date[shows.size()];
		int i=0;
		for(Show show: shows) {
			dates[i] = show.getTime();
			i++;
		}
		return dates;
	}
}
