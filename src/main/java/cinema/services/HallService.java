package cinema.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cinema.dto.HallDTO;
import cinema.entities.Hall;
import cinema.entities.Seat;
import cinema.errorhandler.ResourceNotFoundException;
import cinema.forms.HallForm;
import cinema.repositories.HallRepository;
import cinema.repositories.SeatRepository;

@Service
public class HallService {
	
	@Autowired
	private HallRepository hallRepository;
	
	@Autowired
	private SeatRepository seatRepository;
	
	public HallDTO findHallById(int id) {
		Hall hall = hallRepository.findById(id);
		if(hall==null) {
			throw new ResourceNotFoundException(Hall.class.getSimpleName());
		}
		HallDTO result = new HallDTO.Builder()
				.id(hall.getId())
				.name(hall.getName())
				.nrOfseats(hall.getNumberOfPlaces())
				.seats(convertToSeatMap(hall.getSeats()))
				.create();
		return result;
	}
	
	public List<HallDTO> findAll()  {
		List<Hall> halls = hallRepository.findAll();
		List<HallDTO> toReturn = new ArrayList<HallDTO>();
		for(Hall hall : halls) {
			HallDTO result = new HallDTO.Builder()
					.id(hall.getId())
					.name(hall.getName())
					.nrOfseats(hall.getNumberOfPlaces())
					.seats(convertToSeatMap(hall.getSeats()))
					.create();
			toReturn.add(result);
		}
		return toReturn;
	}
	
	public void deleteHall(int id) {
		hallRepository.delete(id);
	}
	
	public int createHall(HallForm form) {
		Hall hall = new Hall();
		hall.setName(form.getName());
		hall.setNumberOfPlaces(form.getNrOfPlaces());
		Hall result = hallRepository.save(hall);
		if(form.isGenerateSeats()==true) {
			Set<Seat> seats = new HashSet<Seat>();
			for(int i=0; i<form.getNrOfPlaces(); i++) {
				Seat seat = new Seat();
				seat.setHall(result);
				seat.setCode("H" + hall.getId() + "N" + i);
				seatRepository.save(seat);
			}
		}
		return result.getId();
	}
	
	public int updateHall(int id, HallForm form) {
		Hall hall = new Hall();
		hall.setId(id);
		hall.setName(form.getName());
		hall.setNumberOfPlaces(form.getNrOfPlaces());
		Hall result = hallRepository.save(hall);
		return result.getId();
	}
	
	private String[][] convertToSeatMap(Set<Seat> seats) {
		String[][] result = new String[seats.size()][2];
		int i =0;
		for(Seat seat: seats) {
			result[i][0] = Integer.toString(seat.getId());
			result[i][1] = seat.getCode();
			i++;
		}
		return result;
	}
	
	
}
