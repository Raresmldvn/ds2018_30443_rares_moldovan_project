package cinema.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cinema.dto.ReservationDTO;
import cinema.dto.SeatDTO;
import cinema.entities.Hall;
import cinema.entities.Reservation;
import cinema.entities.Seat;
import cinema.errorhandler.ResourceNotFoundException;
import cinema.repositories.HallRepository;
import cinema.repositories.ReservationRepository;
import cinema.repositories.SeatRepository;
import cinema.repositories.ShowRepository;

@Service
public class SeatService {

	@Autowired
	private SeatRepository seatRepository;
	
	@Autowired
	private ReservationRepository reservationRepository;
	
	@Autowired
	private ShowRepository showRepository;
	
	public SeatDTO findSeatById(int id) {
		Seat seat = seatRepository.findById(id);
		if(seat==null) {
			throw new ResourceNotFoundException(Seat.class.getSimpleName());
		}
		SeatDTO result = new SeatDTO.Builder()
				.id(seat.getId())
				.code(seat.getCode())
				.hallId(seat.getHall().getId())
				.hallName(seat.getHall().getName())
				.create();
		return result;
	}
	
	public List<SeatDTO> findAll()  {
		List<Seat> seats = seatRepository.findAll();
		List<SeatDTO> toReturn = new ArrayList<SeatDTO>();
		
		for(Seat seat : seats) {
			SeatDTO result = new SeatDTO.Builder()
					.id(seat.getId())
					.code(seat.getCode())
					.hallId(seat.getHall().getId())
					.hallName(seat.getHall().getName())
					.create();
			toReturn.add(result);
		}
		return toReturn;
	}
	
	public void deleteSeat(int id) {
		seatRepository.delete(id);
	}
	
	public int createSeat(SeatDTO form) {
		
		Seat seat = new Seat();
		seat.setCode(form.getCode());
		seat.setHall(new Hall(form.getHallId(), "", 0));
		Seat result = seatRepository.save(seat);
		return result.getId();
	}
	
	public int updateSeat(int id, SeatDTO form) {
		
		Seat seat = new Seat();
		seat.setId(id);
		seat.setCode(form.getCode());
		seat.setHall(new Hall(form.getHallId(), "", 0));
		Seat result = seatRepository.save(seat);
		return result.getId();
	}
	
	public List<SeatDTO> getRemainingSeats(int showId) {
		List<Reservation> reserved = reservationRepository.findByShow_id(showId);
		Set<Seat> seats = showRepository.findById(showId).getHall().getSeats();
		List<SeatDTO> toReturn = new ArrayList<SeatDTO>();
		for(Seat seat : seats) {
			boolean isReserved = false;
			for(Reservation reservation : reserved) {
				if(reservation.getSeat().getId()==seat.getId()) {
					isReserved = true;
				}
			}
			if(isReserved==false) {
				SeatDTO result = new SeatDTO.Builder()
						.id(seat.getId())
						.code(seat.getCode())
						.hallId(seat.getHall().getId())
						.hallName(seat.getHall().getName())
						.create();
				toReturn.add(result);
			}
		}
		return toReturn;
	}
	
	public List<SeatDTO> getSeatsForHall(int hallId) {
		List<Seat> seats = seatRepository.findByHall_id(hallId);
		List<SeatDTO> toReturn = new ArrayList<SeatDTO>();
		
		for(Seat seat : seats) {
			SeatDTO result = new SeatDTO.Builder()
					.id(seat.getId())
					.code(seat.getCode())
					.hallId(seat.getHall().getId())
					.hallName(seat.getHall().getName())
					.create();
			toReturn.add(result);
		}
		return toReturn;
	}
}
