package cinema.services.util;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cinema.entities.Movie;
import cinema.entities.Show;
import cinema.repositories.MovieRepository;
import cinema.repositories.ReservationRepository;
import cinema.repositories.ShowRepository;

import org.jfree.chart.ChartUtilities;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

@Service
public class ChartService {

	@Autowired
	private MovieRepository movieRepository;
	
	@Autowired
	private ShowRepository showRepository;
	
	@Autowired
	private ReservationRepository reservationRepository;
	
	public void generateChart(String startString, String endString, String name) {
		
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		Date start = null, end = null;
		try {
			start = format.parse(startString + " 00:00");
			end = format.parse(endString + " 24:00");
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		//Get all shows in given interval
		List<Show> shows = showRepository.findAll();
		List<Show> filtered = new ArrayList<Show>();
		for(Show show : shows) {
			if(show.getTime().after(start)&&show.getTime().before(end)) {
				filtered.add(show);
			}
		}
		
		//Get number of reservations for each show
		HashMap<Show, Integer> map = new HashMap<Show, Integer>();
		for(Show show: filtered) {
			map.put(show, reservationRepository.findByShow_id(show.getId()).size());
		}
		
		//Generate movie map
		HashMap<Integer, Integer> movieMap = new HashMap<Integer, Integer>();
		for(Show show : map.keySet()) {
			if(movieMap.get(show.getMovie().getId())==null) {
				movieMap.put(show.getMovie().getId(), map.get(show));
			} else {
				int x = movieMap.get(show.getMovie().getId());
				movieMap.remove(show.getMovie().getId());
				movieMap.put(show.getMovie().getId(), x + map.get(show));
			}
		}
		
		//Create char
		DefaultPieDataset dataset = new DefaultPieDataset( );
		for(Integer i : movieMap.keySet()) {
			dataset.setValue(movieRepository.findById(i).getName(), new Double( i.intValue() ) );
		}
		JFreeChart chart = ChartFactory.createPieChart(
		         "Movie reservation",   // chart title
		         dataset,          // data
		         true,             // include legend
		         true,
		         false);
		         
		int width = 640;   /* Width of the image */
		int height = 480;  /* Height of the image */ 
		File pieChart = new File( name + ".jpeg"); 
		try {
			ChartUtilities.saveChartAsJPEG( pieChart , chart , width , height );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
