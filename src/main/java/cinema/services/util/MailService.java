package cinema.services.util;

import java.io.File;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class MailService {

    @Autowired
    private JavaMailSender emailSender;

    public void sendSimpleMessage(String from, String to, String text, String subject){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setSubject(subject);
        message.setText(text);
        message.setTo(to);
        message.setFrom(from);

        emailSender.send(message);
    }
    
    public void sendMessageWithAttachment(String to, String subject, String text, String pathToAttachment) {
    	
    	try {
    	MimeMessage message = emailSender.createMimeMessage();
    		      
    	MimeMessageHelper helper = new MimeMessageHelper(message, true);
    		     
    	helper.setTo(to);
    	helper.setSubject(subject);
    	helper.setText(text);
    		         
    	FileSystemResource file  = new FileSystemResource(new File(pathToAttachment));
    	helper.addAttachment("Ticket.pdf", file);
    		 
    	emailSender.send(message);
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
    	
    	}
}
