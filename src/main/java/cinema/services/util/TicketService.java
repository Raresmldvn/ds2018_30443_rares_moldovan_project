package cinema.services.util;

import org.springframework.stereotype.Service;

import cinema.entities.Reservation;

import java.awt.Color;
import java.awt.Rectangle;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

@Service
public class TicketService {

	public String createPDFTicket(Reservation reservation) {
		PDDocument document = new PDDocument();
		PDPage page = new PDPage();
		document.addPage(page);
		 
		try {
		PDPageContentStream contentStream = new PDPageContentStream(document, page);
		contentStream.setFont(PDType1Font.COURIER, 16);
		drawRect(contentStream, Color.BLACK, new Rectangle(100, 720, 320, -200), false);
		contentStream.beginText();
	
		contentStream.newLineAtOffset(100, 700);
		contentStream.showText("RESERVATION NO. " + reservation.getId());
		
		contentStream.newLineAtOffset(0, -20);
		contentStream.showText(reservation.getUser().getName());
		
		contentStream.setFont(PDType1Font.COURIER, 12);
		
		contentStream.newLineAtOffset(0, -15);
		contentStream.showText("Movie: " + reservation.getShow().getMovie().getName());
		
		contentStream.newLineAtOffset(0, -15);
		contentStream.showText("Hall : " + reservation.getShow().getHall().getName());
		
		contentStream.newLineAtOffset(0, -15);
		contentStream.showText("Seat : " + reservation.getSeat().getCode());
		
		contentStream.newLineAtOffset(0, -15);
		contentStream.showText("Date: " + reservation.getShow().getTime());
		
		contentStream.endText();
		contentStream.close();
		document.save("Reservation" + reservation.getId() + ".pdf");
		document.close();
		return "Reservation" + reservation.getId() + ".pdf";
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	void drawRect(PDPageContentStream content,Color color, Rectangle rect, boolean fill) throws IOException {
	    content.addRect(rect.x, rect.y, rect.width, rect.height);
	    if (fill) {
	        content.setNonStrokingColor(color);
	        content.fill();
	    } else {
	        content.setStrokingColor(color);
	        content.stroke();
	    }
	}
}
