package cinema.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cinema.dto.GenreDTO;
import cinema.entities.Genre;
import cinema.entities.Movie;
import cinema.errorhandler.ResourceNotFoundException;
import cinema.repositories.GenreRepository;

@Service
public class GenreService {

	@Autowired
	private GenreRepository genreRepository;
	
	public GenreDTO findGenreById(int id) {
		Genre genre = genreRepository.findById(id);
		if(genre==null) {
			throw new ResourceNotFoundException(Genre.class.getSimpleName());
		}
		GenreDTO result = new GenreDTO.Builder()
				.id(genre.getId())
				.name(genre.getName())
				.movies(extractGenreNames(genre.getMovies()))
				.create();
		return result;
	}
	
	public List<GenreDTO> findAll() {
		List<Genre> genres= genreRepository.findAll();
		List<GenreDTO> toReturn = new ArrayList<GenreDTO>();
		for (Genre genre: genres) {
			GenreDTO result = new GenreDTO.Builder()
					.id(genre.getId())
					.name(genre.getName())
					.movies(extractGenreNames(genre.getMovies()))
					.create();
			toReturn.add(result);
		}
		return toReturn;
	}
	
	public void deleteGenre(int id) {
		genreRepository.delete(id);
	}
	
	public int createGenre(String name) {
		Genre genre = new Genre();
		genre.setName(name);
		Genre result = genreRepository.save(genre);
		return result.getId();
	}
	
	public int updateGenre(int id, String name) {
		Genre genre = new Genre();
		genre.setId(id);
		genre.setName(name);
		Genre result = genreRepository.save(genre);
		return result.getId();
	}
	
	private String[] extractGenreNames(Set<Movie> movies) {
		String[] names = new String[movies.size()];
		int i=0;
		for(Movie movie : movies) {
			names[i] = movie.getName();
			i++;
		}
		return names;
	}
}
