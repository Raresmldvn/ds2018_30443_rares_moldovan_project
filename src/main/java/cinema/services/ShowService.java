package cinema.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cinema.dto.ShowDTO;
import cinema.entities.Hall;
import cinema.entities.Movie;
import cinema.entities.Reservation;
import cinema.entities.Show;
import cinema.entities.User;
import cinema.errorhandler.EntityValidationException;
import cinema.errorhandler.ResourceNotFoundException;
import cinema.repositories.ReservationRepository;
import cinema.repositories.ShowRepository;

@Service
public class ShowService {

	@Autowired
	private ShowRepository showRepository;
	
	@Autowired
	private ReservationRepository reservationRepository;
	
	public ShowDTO findShowById(int id) {
		Show show = showRepository.findById(id);
		if(show==null) {
			throw new ResourceNotFoundException(Show.class.getSimpleName());
		}
		ShowDTO result = new ShowDTO.Builder()
				.id(show.getId())
				.movieId(show.getMovie().getId())
				.movieName(show.getMovie().getName())
				.hallId(show.getHall().getId())
				.hallName(show.getHall().getName())
				.date(show.getTime())
				.create();
		return result;
	}
	
	public List<ShowDTO> findAll()  {
		List<Show> shows = showRepository.findAll();
		List<ShowDTO> toReturn = new ArrayList<ShowDTO>();
		for(Show show : shows) {
			ShowDTO result = new ShowDTO.Builder()
					.id(show.getId())
					.movieId(show.getMovie().getId())
					.movieName(show.getMovie().getName())
					.hallId(show.getHall().getId())
					.hallName(show.getHall().getName())
					.date(show.getTime())
					.create();
			toReturn.add(result);
		}
		return toReturn;
	}
	
	public int numberOfSeatsLeft(int showId) {
		Show show = showRepository.findById(showId);
		int total = show.getHall().getNumberOfPlaces();
		List<Reservation> reservations = reservationRepository.findByShow_id(show.getId());
		return total - reservations.size();
	}
	
	public void deleteShow(int id) {
		showRepository.delete(id);
	}
	
	public int createShow(ShowDTO form) {
		Show show = new Show();
		show.setHall(new Hall(form.getHallId(), "", 0));
		Movie movie = new Movie();
		movie.setId(form.getMovieId());
		show.setMovie(movie);
		show.setTime(form.getDate());
		if(validateShowConflict(show)==false) {
			List<String> errors = new ArrayList<String>();
			errors.add("Date is conflicting or passed");
			throw new EntityValidationException(User.class.getSimpleName(), errors);
		}
		Show result =  showRepository.save(show);
		return result.getId();
	}
	
	public int updateShow(int id, ShowDTO form) {
		Show show = new Show();
		show.setId(id);
		show.setHall(new Hall(form.getHallId(), "", 0));
		Movie movie = new Movie();
		movie.setId(form.getMovieId());
		show.setMovie(movie);
		show.setTime(form.getDate());
		Show result =  showRepository.save(show);
		return result.getId();
	}
	
	private boolean validateShowConflict(Show show) {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);

		Date today = c.getTime();
		System.out.println(today.toString() + " " + show.getTime().toString());
		if(show.getTime().before(today)) {
			return false;
		}
		List<Show> conflicting = showRepository.findByHall_Id(show.getHall().getId());
		for(Show conflictingShow : conflicting) {
			if(show.getTime().compareTo(conflictingShow.getTime())==0) {
				return false;
			}
		}
		return true;
	}
	
	public List<ShowDTO> getShowsForMovie(int movieId) {
		List<Show> shows = showRepository.findByMovie_Id(movieId);
		List<ShowDTO> toReturn = new ArrayList<ShowDTO>();
		for(Show show : shows) {
			ShowDTO result = new ShowDTO.Builder()
					.id(show.getId())
					.movieId(show.getMovie().getId())
					.movieName(show.getMovie().getName())
					.hallId(show.getHall().getId())
					.hallName(show.getHall().getName())
					.date(show.getTime())
					.create();
			toReturn.add(result);
		}
		return toReturn;
	}
}
