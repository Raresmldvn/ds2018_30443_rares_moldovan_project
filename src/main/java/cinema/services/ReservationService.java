package cinema.services;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cinema.dto.ReservationDTO;
import cinema.entities.Hall;
import cinema.entities.Reservation;
import cinema.entities.Seat;
import cinema.entities.Show;
import cinema.entities.User;
import cinema.errorhandler.EntityValidationException;
import cinema.errorhandler.ResourceNotFoundException;
import cinema.repositories.HallRepository;
import cinema.repositories.ReservationRepository;
import cinema.repositories.SeatRepository;
import cinema.repositories.ShowRepository;
import cinema.services.util.MailService;
import cinema.services.util.TicketService;

@Service
public class ReservationService {

	@Autowired
	private ReservationRepository reservationRepository;
	
	@Autowired
	private ShowRepository showRepository;
	
	@Autowired
	private MailService mailService;
	
	@Autowired
	private TicketService ticketService;
	
	public ReservationDTO findReservationById(int id) {
		Reservation reservation = reservationRepository.findById(id);
		if(reservation==null) {
			throw new ResourceNotFoundException(Reservation.class.getSimpleName());
		}
		ReservationDTO result = new ReservationDTO.Builder()
				.id(reservation.getId())
				.hallId(reservation.getShow().getHall().getId())
				.hallName(reservation.getShow().getHall().getName())
				.seatId(reservation.getSeat().getId())
				.movieName(reservation.getShow().getMovie().getName())
				.showId(reservation.getShow().getId())
				.showDate(reservation.getShow().getTime())
				.userId(reservation.getUser().getId())
				.username(reservation.getUser().getName())
				.create();
		return result;
	}
	
	public List<ReservationDTO> findAll()  {
		List<Reservation> reservations = reservationRepository.findAll();
		List<ReservationDTO> toReturn = new ArrayList<ReservationDTO>();
		for(Reservation reservation : reservations) {
			ReservationDTO result = new ReservationDTO.Builder()
					.id(reservation.getId())
					.hallId(reservation.getShow().getHall().getId())
					.hallName(reservation.getShow().getHall().getName())
					.seatId(reservation.getSeat().getId())
					.movieName(reservation.getShow().getMovie().getName())
					.showId(reservation.getShow().getId())
					.showDate(reservation.getShow().getTime())
					.userId(reservation.getUser().getId())
					.username(reservation.getUser().getName())
					.create();
			toReturn.add(result);
		}
		return toReturn;
	}
	
	public void deleteReservation(int id) {
		reservationRepository.delete(id);
	}
	
	public int createReservation(ReservationDTO form) {
		Reservation reservation = new Reservation();
		User user = new User();
		user.setId(form.getUserId());
		Show show = new Show();
		show.setId(form.getShowId());
		Seat seat = new Seat();
		seat.setId(form.getSeatId());
		reservation.setUser(user);
		reservation.setSeat(seat);
		reservation.setShow(show);
		if(validateSeat(showRepository.findById(form.getShowId()).getHall(), form.getSeatId())==false) {
			List<String> validationErrors = new ArrayList<String>();
			validationErrors.add("Seat does not belong to hall!");
			throw new EntityValidationException(Reservation.class.getSimpleName(),validationErrors);
		}
		if(validateSeatNotTaken(reservationRepository.findByShow_id(form.getShowId()), form.getSeatId())==false) {
			List<String> validationErrors = new ArrayList<String>();
			validationErrors.add("Seat is taken!");
			throw new EntityValidationException(Reservation.class.getSimpleName(),validationErrors);
		}
		Reservation result = reservationRepository.save(reservation);
		reservation.setId(result.getId());
		result = reservationRepository.findById(result.getId());
		String location = ticketService.createPDFTicket(result);
		mailService.sendMessageWithAttachment(result.getUser().getEmail(), "Cinema ticket has arrived", 
				"This is your ticket for reservation no. " + result.getId(), location);
		File file = new File(location);
		file.delete();
		return result.getId();
	}
	
	public int updateReservation(int id, ReservationDTO form) {
		Reservation reservation = new Reservation();
		reservation.setId(id);
		User user = new User();
		user.setId(form.getUserId());
		Show show = new Show();
		show.setId(form.getShowId());
		Seat seat = new Seat();
		seat.setId(form.getSeatId());
		reservation.setUser(user);
		reservation.setSeat(seat);
		reservation.setShow(show);
		Reservation result = reservationRepository.save(reservation);
		return result.getId();
	}
	
	private boolean validateSeat(Hall hall, int seatId) {
		Set<Seat> seats = hall.getSeats();
		for(Seat seat : seats) {
			if(seat.getId()==seatId) {
				return true;
			}
		}
		return false;
	}
	
	private boolean validateSeatNotTaken(List<Reservation> reservations, int seatId) {
		for(Reservation reservation : reservations) {
			if(reservation.getSeat().getId()==seatId) {
				return false;
			}
		}
		return true;
	}
}
