package cinema.services.filter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import cinema.entities.Movie;
import cinema.entities.Show;

public class DateFilter extends Filter{

	private String date;
	public DateFilter(List<Movie> list, String date) {
		super(list);
		this.date = date;
	}
	@Override
	public List<Movie> filter() {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date theDate = null;
		try {
			theDate = format.parse(date);
		} catch (ParseException e) {
			return list;
		}
		List<Movie> filtered = new ArrayList<Movie>();
		for(Movie movie : list) {
			Set<Show> shows = movie.getShows();
			for(Show show : shows) {
				if(checkSameDay(show.getTime(), theDate)) {
					filtered.add(movie);
					break;
				}
			}
		}
		return filtered;
	}
	
	private boolean checkSameDay(Date date1, Date date2) {
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(date1);
		cal2.setTime(date2);
		return cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
		                  cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
	}

	
}
