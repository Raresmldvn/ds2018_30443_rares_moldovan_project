package cinema.services.filter;

import java.util.List;

import cinema.entities.Movie;

public abstract class Filter {

	protected List<Movie> list;
	
	public Filter(List<Movie> list) {
		this.list = list;
	}
	
	public void setList(List<Movie> list) {
		this.list = list;
	}
	
	public abstract List<Movie> filter();
}
