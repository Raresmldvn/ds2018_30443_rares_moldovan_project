package cinema.services.filter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import cinema.entities.Genre;
import cinema.entities.Movie;

public class GenreFilter extends Filter {

	private int genreId;
	public GenreFilter(List<Movie> list, int genreId) {
		super(list);
		this.genreId = genreId;
	}

	@Override
	public List<Movie> filter() {
		if(genreId<=0) {
			return list;
		}
		List<Movie> filtered = new ArrayList<Movie>();
		for(Movie movie : list) {
			Set<Genre> genres = movie.getGenres();
			for(Genre genre : genres) {
				if(genre.getId()==genreId) {
					filtered.add(movie);
					break;
				}
			}
		}
		return filtered;
	}

	
}
