package cinema.services.filter;

import java.util.ArrayList;
import java.util.List;

import cinema.entities.Movie;

public class YearFilter extends Filter{

	private int year;
	public YearFilter(List<Movie> list, int year) {
		super(list);
		this.year = year;
	}

	@Override
	public List<Movie> filter() {
		if(year<=0) {
			return list;
		}
		List<Movie> filtered = new ArrayList<Movie>();
		for(Movie movie : list) {
			if(movie.getYear()==year) {
				filtered.add(movie);
			}
		}
		return filtered;
	}

	
}
