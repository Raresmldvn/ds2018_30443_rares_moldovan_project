package cinema.services.filter;

import java.util.ArrayList;
import java.util.List;

import cinema.entities.Movie;

public class PaginationFilter extends Filter{

	public static final int PAGE_SIZE = 5;
	
	private int page;
	public PaginationFilter(List<Movie> list, int page) {
		super(list);
		this.page = page;
	}

	@Override
	public List<Movie> filter() {
		if(page==0) {
			page=1;
		}
		int offset = (page-1)*PAGE_SIZE;
		if(offset>=list.size() || page<0) {
			return new ArrayList<Movie>();
		}
		int limit = PAGE_SIZE;
		int stop = offset + limit;
		if(offset + limit >= list.size()) {
			stop = list.size();
		}
		System.out.println("" + offset + " " + stop);
		List<Movie> filtered = new ArrayList<Movie>();
		for(int i = offset; i<stop; i++) {
			filtered.add(list.get(i));
		}
		return filtered;
	}

	
}
