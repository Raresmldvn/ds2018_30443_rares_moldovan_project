package cinema.es.model;

public class MovieModel {

	private int id;
	private String description;
	private String title;
	
	public MovieModel() {}

	
	public MovieModel(int id, String description, String title) {
		super();
		this.id = id;
		this.description = description;
		this.title = title;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	
}
