package cinema.es.helper.model;

public class MultiMatch {

	private String query;
	private String[] fields = {"title", "description"};
	private String fuzziness = "AUTO";
	public void setQuery(String query) {
		this.query = query;
	}
	public String[] getFields() {
		return fields;
	}
	public void setFields(String[] fileds) {
		this.fields = fileds;
	}
	public String getQuery() {
		return query;
	}
	public String getFuzziness() {
		return fuzziness;
	}
	public void setFuzziness(String fuzziness) {
		this.fuzziness = fuzziness;
	}
}
