package cinema.es.helper.model;

public class SearchBody {

	private Query query;
	
	public SearchBody(Query query) {
		this.query = query;
	}

	public Query getQuery() {
		return query;
	}

	public void setQuery(Query query) {
		this.query = query;
	}
	
	
}
