package cinema.es.helper;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import cinema.es.helper.model.MultiMatch;
import cinema.es.helper.model.Query;
import cinema.es.helper.model.SearchBody;
import cinema.es.model.MovieModel;

public class Transport {

	public static final String CINEMA_URL = "http://localhost:9200/cinema/movie";

	
	private HttpURLConnection connection;
	
	public Transport() {
	}
	public boolean indexMovie(MovieModel model) {
		try {
			URL url = new URL(CINEMA_URL + "/" + model.getId());
			connection = (HttpURLConnection) url.openConnection();
			ObjectMapper objectMapper = new ObjectMapper();
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			OutputStream outputStream = connection.getOutputStream();
			outputStream.write(objectMapper.writeValueAsBytes(model));
			outputStream.close();
			String response = readResponse();
			ObjectNode objectNode = objectMapper.readValue(response, ObjectNode.class);
			if(objectNode.has("result") && "created".equals(objectNode.get("result").asText())) {
				return true;
			}
			return false;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public int[] searchMovie(String queryString) {
		try {
		URL url = new URL(CINEMA_URL + "/_search");
		connection = (HttpURLConnection) url.openConnection();
		ObjectMapper objectMapper = new ObjectMapper();
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);
		MultiMatch mm = new MultiMatch();
		mm.setQuery(queryString);
		Query query = new Query();
		query.setMulti_match(mm);
		SearchBody sb = new SearchBody(query);
		System.out.println(objectMapper.writeValueAsString(sb));
		OutputStream outputStream = connection.getOutputStream();
		outputStream.write(objectMapper.writeValueAsBytes(sb));
		outputStream.close();
		String response = readResponse();
		return readResultIds(response, objectMapper);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String readResponse() {
		try {
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String inputLine;
		StringBuffer content = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}
		in.close();
		return content.toString();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private int[] readResultIds(String response, ObjectMapper objectMapper) {
		try {
			 JsonNode objectNode = objectMapper.readValue(response, JsonNode.class);
			JsonNode hits = objectNode.get("hits");
			if(hits.get("total").asInt()==0) {
				return null;
			}
			int[] ids = new int[hits.get("total").asInt()];
			double[] relevance = new double[hits.get("total").asInt()];
			JsonNode hitList = hits.get("hits");
			int i=0;
			for(JsonNode result : hitList) {
				ids[i] = result.get("_id").asInt();
				relevance[i] = result.get("_score").asDouble();
				System.out.println(ids[i] + " " + relevance[i]);
				i++;
			}
			return sortByRelevance(ids, relevance);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private int[] sortByRelevance(int[] ids, double relevance[]) {
		int n = ids.length;
		boolean changed = true;
		while(changed) {
			changed = false;
			for(int i=0; i<n-1; i++) {
				if(relevance[i]>relevance[i+1]) {
					double aux = relevance[i];
					relevance[i] = relevance[i+1];
					relevance[i+1] = aux; 
					int auxInt = ids[i+1];
					ids[i+1] = ids[i];
					ids[i] = auxInt;
					changed = true;
				}
			}
		}
		return ids;
	}
	
}
