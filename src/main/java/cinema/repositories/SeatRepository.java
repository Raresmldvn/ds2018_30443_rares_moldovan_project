package cinema.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cinema.entities.Seat;

public interface SeatRepository extends JpaRepository<Seat, Integer> {

	Seat findById(int id);
	List<Seat> findByHall_id(int id);
	
}

