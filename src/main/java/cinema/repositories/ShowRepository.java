package cinema.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cinema.entities.Show;

public interface ShowRepository extends JpaRepository<Show, Integer> {

	Show findById(int id);
	
	List<Show> findByHall_Id(int id);
	
	List<Show> findByMovie_Id(int id);
}

