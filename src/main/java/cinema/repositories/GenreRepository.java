package cinema.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import cinema.entities.Genre;

public interface GenreRepository extends JpaRepository<Genre, Integer> {

	Genre findByName(String name);

	Genre findById(int id);
	
}

