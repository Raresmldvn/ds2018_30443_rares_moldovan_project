package cinema.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cinema.entities.Movie;

public interface MovieRepository extends JpaRepository<Movie, Integer> {

	Movie findByName(String name);

	Movie findById(int id);
	
	List<Movie> findAllByOrderByYearAsc(); 
	
	List<Movie> findAllByOrderByYearDesc(); 
}
