package cinema.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import cinema.entities.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	User findByName(String name);

	Optional<User> findByEmail(String email);
	
	User findById(int id);
	
}
