package cinema.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import cinema.entities.Hall;

public interface HallRepository extends JpaRepository<Hall, Integer> {

	Hall findByName(String name);

	Hall findById(int id);
	
}
