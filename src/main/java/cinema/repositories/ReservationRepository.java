package cinema.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cinema.entities.Reservation;

public interface ReservationRepository extends JpaRepository<Reservation, Integer> {

	Reservation findById(int id);
	
	List<Reservation> findByShow_id(int showId);
	
	List<Reservation> findByUser_id(int showId);
}
