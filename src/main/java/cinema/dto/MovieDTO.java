package cinema.dto;

import java.util.Date;

public class MovieDTO {

	private int id;
	private String name;
	private String description;
	private int year;
	private String image;
	private String[] genres;
	private Date[] shows;
	
	public MovieDTO(int id, String name, String description, int year, String image, String[] genres, Date[] shows) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.year = year;
		this.image = image;
		this.genres = genres;
		this.shows = shows;

	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String[] getGenres() {
		return genres;
	}
	public void setGenres(String[] genres) {
		this.genres = genres;
	}
	
	public Date[] getShows() {
		return shows;
	}
	public void setShows(Date[] shows) {
		this.shows = shows;
	}


	public static class Builder {
		private int nestedid;
		private String nestedname;
		private String nesteddescription;
		private String nestedimage;
		private int nestedyear;
		private String[] nestedgenres;
		private Date[] nestedshows;
		
		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}
		
		public Builder name(String name) {
			this.nestedname = name;
			return this;
		}
		
		public Builder description(String description) {
			this.nesteddescription = description;
			return this;
		}
		
		public Builder image(String image) {
			this.nestedimage = image;
			return this;
		}
		
		public Builder year(int year) {
			this.nestedyear = year;
			return this;
		}
		
		public Builder genres(String[] genres) {
			this.nestedgenres = genres;
			return this;
		}
		
		public Builder shows(Date[] shows) {
			this.nestedshows = shows;
			return this;
		}
		
		public MovieDTO create() {
			return new MovieDTO(nestedid, nestedname, nesteddescription, nestedyear, nestedimage,nestedgenres, nestedshows);
		}
	}
}
