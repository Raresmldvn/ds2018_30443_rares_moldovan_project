package cinema.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ShowDTO {
	
	private int id;
	private int movieId;
	private String movieName;
	private int hallId;
	private String hallName;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd hh:mm")
	private Date date;
	
	public ShowDTO() {}
	
	public ShowDTO(int id, int movieId, String movieName, int hallId, String hallName, Date date) {
		super();
		this.id = id;
		this.movieId = movieId;
		this.movieName = movieName;
		this.hallId = hallId;
		this.hallName = hallName;
		this.date = date;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMovieId() {
		return movieId;
	}
	public void setMovieId(int movieId) {
		this.movieId = movieId;
	}
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public int getHallId() {
		return hallId;
	}
	public void setHallId(int hallId) {
		this.hallId = hallId;
	}
	public String getHallName() {
		return hallName;
	}
	public void setHallName(String hallName) {
		this.hallName = hallName;
	}
	
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}


	public static class Builder {
		private Integer nestedid;
		private int nestedmovieid;
		private String nestedmoviename;
		private int nestedhallid;
		private String nestedhallname;
		private Date nesteddate;
		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}

		public Builder movieId(int id) {
			this.nestedmovieid = id;
			return this;
		}
		
		public Builder movieName(String name) {
			this.nestedmoviename = name;
			return this;
		}

		public Builder email(int id) {
			this.nestedhallid = id;
			return this;
		}

		public Builder hallId(int id) {
			this.nestedhallid =id;
			return this;
		}

		public Builder hallName(String name) {
			this.nestedhallname = name;
			return this;
		}

		public Builder date(Date date) {
			this.nesteddate = date;
			return this;
		}
		
		public ShowDTO create() {
			return new ShowDTO(nestedid, nestedmovieid, nestedmoviename, nestedhallid, nestedhallname, nesteddate);
		}

	}
}
