package cinema.dto;

import java.util.Date;

public class ReservationDTO {

	private int id;
	private int userId;
	private String userName;
	private int showId;
	private String movieName;
	private Date showDate;
	private int hallId;
	private String hallName;
	private int seatId;
	
	public ReservationDTO() {}
	public ReservationDTO(int id, int userId, String userName, int showId, String movieName, Date showDate, int hallId,
			String hallName, int seatId) {
		super();
		this.id = id;
		this.userId = userId;
		this.userName = userName;
		this.showId = showId;
		this.movieName = movieName;
		this.showDate = showDate;
		this.hallId = hallId;
		this.hallName = hallName;
		this.seatId = seatId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getShowId() {
		return showId;
	}

	public void setShowId(int showId) {
		this.showId = showId;
	}

	public String getMovieName() {
		return movieName;
	}

	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}

	public Date getShowDate() {
		return showDate;
	}

	public void setShowDate(Date showDate) {
		this.showDate = showDate;
	}

	public int getHallId() {
		return hallId;
	}

	public void setHallId(int hallId) {
		this.hallId = hallId;
	}

	public String getHallName() {
		return hallName;
	}

	public void setHallName(String hallName) {
		this.hallName = hallName;
	}
	
	
	public int getSeatId() {
		return seatId;
	}

	public void setSeatId(int seatId) {
		this.seatId = seatId;
	}


	public static class Builder {
		
		private int nestedid;
		private int nesteduserid;
		private String nestedusername;
		private int nestedshowid;
		private String nestedmoviename;
		private Date nestedshowdate;
		private int nestedhallid;
		private String nestedhallname;
		private int nestedseatid;
		
		
		public Builder id(int nestedid) {
			this.nestedid = nestedid;
			return this;
		}
		
		public Builder userId(int nesteduserid) {
			this.nesteduserid = nesteduserid;
			return this;
		}
		public Builder username(String nestedusername) {
			this.nestedusername = nestedusername;
			return this;
		}
		public Builder showId(int nestedshowid) {
			this.nestedshowid = nestedshowid;
			return this;
		}
		public Builder movieName(String nestedmoviename) {
			this.nestedmoviename = nestedmoviename;
			return this;
		}
		public Builder showDate(Date nestedshowdate) {
			this.nestedshowdate = nestedshowdate;
			return this;
		}
		public Builder hallId(int nestedhallid) {
			this.nestedhallid = nestedhallid;
			return this;
		}
		public Builder hallName(String nestedhallname) {
			this.nestedhallname = nestedhallname;
			return this;
		}
		
		public Builder seatId(int seatId) {
			this.nestedseatid = seatId;
			return this;
		}
		
		
		public ReservationDTO create() {
			return new ReservationDTO(nestedid, nesteduserid, nestedusername, nestedshowid, nestedmoviename, nestedshowdate, nestedhallid, nestedhallname, nestedseatid);
		}
	}
}
