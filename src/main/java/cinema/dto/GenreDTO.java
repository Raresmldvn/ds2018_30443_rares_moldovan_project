package cinema.dto;

public class GenreDTO {

	private int id;
	private String name;
	private String[] movies;
	
	public GenreDTO(int id, String name, String[] movies) {
		super();
		this.id = id;
		this.name = name;
		this.movies = movies;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String[] getMovies() {
		return movies;
	}
	
	public void setMovies(String[] movies) {
		this.movies = movies;
	}
	
	public static class Builder {
		private int nestedid;
		private String nestedname;
		private String[] nestedmovies;
		
		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}
		
		public Builder name(String name) {
			this.nestedname = name;
			return this;
		}
		
		public Builder movies(String[] movies) {
			this.nestedmovies = movies;
			return this;
		}
		
		public GenreDTO create() {
			return new GenreDTO(nestedid, nestedname, nestedmovies);
		}
	}
}
