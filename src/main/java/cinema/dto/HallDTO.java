package cinema.dto;

public class HallDTO {

	private int id;
	private String name;
	private int nrSeats;
	private String[][] seats;
	public HallDTO(int id, String name, int nrSeats, String[][] seats) {
		super();
		this.id = id;
		this.name = name;
		this.nrSeats = nrSeats;
		this.seats = seats;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNrSeats() {
		return nrSeats;
	}
	public void setNrSeats(int nrSeats) {
		this.nrSeats = nrSeats;
	}
	public String[][] getSeats() {
		return seats;
	}
	public void setSeats(String[][] seats) {
		this.seats = seats;
	}
	
	public static class Builder {
		
		private int nestedid;
		private String nestedname;
		private int nestednrofseats;
		private String[][] nestedseats;
		
		public Builder id(int nestedid) {
			this.nestedid = nestedid;
			return this;
		}
		public Builder name(String nestedname) {
			this.nestedname = nestedname;
			return this;
		}
		public Builder nrOfseats(int nestednrofseats) {
			this.nestednrofseats = nestednrofseats;
			return this;
		}
		public Builder seats(String[][] nestedseats) {
			this.nestedseats = nestedseats;
			return this;
		}
		
		public HallDTO create() {
			return new HallDTO(nestedid, nestedname, nestednrofseats, nestedseats);
		}
	}
}
