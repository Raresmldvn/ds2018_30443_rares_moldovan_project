package cinema.dto;

public class SeatDTO {

	private Integer id;
	private Integer hallId;
	private String hallName;
	private String code;
	
	public SeatDTO() {}
	public SeatDTO(int id, int hallId, String hallName, String code) {
		super();
		this.id = id;
		this.hallId = hallId;
		this.hallName = hallName;
		this.code = code;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getHallId() {
		return hallId;
	}

	public void setHallId(int hallId) {
		this.hallId = hallId;
	}

	public String getHallName() {
		return hallName;
	}

	public void setHallName(String hallName) {
		this.hallName = hallName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public static class Builder {
		
		private int nestedid;
		private int nestedhallid;
		private String nestedhallname;
		private String nestedcode;
		
		public int getNestedid() {
			return nestedid;
		}
		public Builder id(int nestedid) {
			this.nestedid = nestedid;
			return this;
		}
		
		public Builder hallId(int nestedhallid) {
			this.nestedhallid = nestedhallid;
			return this;
		}
		
		public Builder hallName(String nestedhallname) {
			this.nestedhallname = nestedhallname;
			return this;
		}

		public Builder code(String nestedcode) {
			this.nestedcode = nestedcode;
			return this;
		}
		
		public SeatDTO create() {
			return new SeatDTO(nestedid, nestedhallid, nestedhallname, nestedcode);
		}
	}
}
