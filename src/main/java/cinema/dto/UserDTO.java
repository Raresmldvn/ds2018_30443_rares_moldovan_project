package cinema.dto;

public class UserDTO {

	private Integer id;
	private String firstname;
	private String surname;
	private String email;
	private String password;
	private boolean isAdmin;

	public UserDTO() {
	}

	public UserDTO(Integer id, String firstname, String surname,  String email, String password, boolean isAdmin) {
		super();
		this.id = id;
		this.firstname= firstname;
		this.surname = surname;
		this.email = email;
		this.password = password;
		this.isAdmin = isAdmin;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}



	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}




	public static class Builder {
		private Integer nestedid;
		private String nestedfirstname;
		private String nestedsurname;
		private String nestedemail;
		private String nestedpassword;
		private boolean nestedisadmin;

		public Builder id(int id) {
			this.nestedid = id;
			return this;
		}

		public Builder firstname(String name) {
			this.nestedfirstname = name;
			return this;
		}
		
		public Builder surname(String name) {
			this.nestedsurname = name;
			return this;
		}

		public Builder email(String email) {
			this.nestedemail = email;
			return this;
		}

		public Builder password(String password) {
			this.nestedpassword = password;
			return this;
		}

		public Builder isAdmin(boolean isAdmin) {
			this.nestedisadmin = isAdmin;
			return this;
		}

		public UserDTO create() {
			return new UserDTO(nestedid, nestedfirstname,nestedsurname, nestedemail, nestedpassword, nestedisadmin);
		}

	}

}
