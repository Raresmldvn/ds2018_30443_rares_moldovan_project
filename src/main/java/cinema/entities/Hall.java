package cinema.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "hall")
public class Hall {

	private int id;
	private String name;
	private int numberOfPlaces;
	private Set<Show> shows;
	private Set<Seat> seats;
	
	public Hall() {}

	public Hall(int id, String name, int numberOfPlaces) {
		super();
		this.id = id;
		this.name = name;
		this.numberOfPlaces = numberOfPlaces;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 255)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "nr_of_places", unique = false, nullable = false)
	public int getNumberOfPlaces() {
		return numberOfPlaces;
	}

	public void setNumberOfPlaces(int numberOfPlaces) {
		this.numberOfPlaces = numberOfPlaces;
	}
	
	@OneToMany(mappedBy="hall", fetch=FetchType.EAGER)
	public Set<Show> getShows() {
		return shows;
	}

	public void setShows(Set<Show> shows) {
		this.shows = shows;
	}
	
	@OneToMany(mappedBy="hall", fetch=FetchType.EAGER)
	public Set<Seat> getSeats() {
		return seats;
	}

	public void setSeats(Set<Seat> seats) {
		this.seats = seats;
	}
	
}
